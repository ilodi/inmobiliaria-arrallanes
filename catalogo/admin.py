from django.contrib import admin
from .models import Catalogo, Characteristics, Tipos_de_inmuebles
# Register your models here.
class CatalogoAdmin(admin.ModelAdmin):
    # FILTROS Extras Cool 
    readonly_fields = ('created', 'updated')
    list_display = ('title', 'author', 'Idcontrol', 'numcontrol', 'published')
    ordering = ('author' , 'published')
    search_fields = ('title', 'author__username', 'characteristics__name', 'Idcontrol', 'numcontrol', )
    date_hierarchy = ('published')
    list_filter = ('author__username', 'typeC__name' )
    def get_readonly_fields(self, request, obj=None):
        if request.user.groups.filter(name="Personal").exists():
            return  ('created', 'updated')
        else:
            return  ('created', 'updated')

class CharacteristicsAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

class Tipos_de_inmueblesAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

admin.site.register(Catalogo,CatalogoAdmin)
admin.site.register(Characteristics,CharacteristicsAdmin)
admin.site.register(Tipos_de_inmuebles,Tipos_de_inmueblesAdmin)