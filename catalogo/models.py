from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField 

# Create 1 models
class Characteristics(models.Model):
    name = models.CharField(max_length=150, verbose_name = "Nombre de la Característica")
    created = models.DateTimeField(auto_now_add=True, verbose_name = "Fecha de Creación")
    updated = models.DateTimeField(auto_now=True, verbose_name = "Fecha de Edición")

    class Meta:
        verbose_name = "Característica"
        verbose_name_plural = "Características"
        ordering = ["-created"]
    
    def __str__(self):
        return self.name

class Tipos_de_inmuebles(models.Model):
    name = models.CharField(max_length=150, verbose_name = "Tipo de Inmueble", default='Otros')
    created = models.DateTimeField(auto_now_add=True, verbose_name = "Fecha de Creación")
    updated = models.DateTimeField(auto_now=True, verbose_name = "Fecha de Edición")
   
    class Meta:
        verbose_name = "Tipo de Inmueble"
        verbose_name_plural = "Tipos de Inmuebles"
        ordering = ["-created"]
    
    def __str__(self):
        return self.name

# Create 2 models
class Catalogo(models.Model):
    TYPE_COUNTRY = (
        ('M' , 'México'),
        ('O' , 'Otro'),
    )
    highlight = models.BooleanField(default=False, verbose_name = "Propiedad Destacada")
    title = models.CharField(max_length=150, verbose_name = "Título")
    subtitle = models.CharField(max_length=150, verbose_name = "Subtítulo")
    description = RichTextField(verbose_name = "Descripción")
    published = models.DateTimeField(verbose_name="Fecha de publicación", default=now)
    author =  models.ForeignKey(User, verbose_name="Vendedor", on_delete=models.PROTECT, blank=True, null=True)
    characteristics = models.ManyToManyField(Characteristics, verbose_name="Características" )
    typeC  = models.ManyToManyField(Tipos_de_inmuebles, verbose_name = "Tipo de Inmueble", related_name="get_posts" )
    numcontrol =  models.IntegerField(unique=True, verbose_name = "Número de Control")
    Idcontrol = models.IntegerField(unique=True, verbose_name = "Código de Control")
    Rentprice =  models.IntegerField(blank=True, null=True, verbose_name = "Precio Renta")
    Sellprice = models.IntegerField(blank=True, null=True, verbose_name = "Precio Venta")
    googleS = models.URLField(blank=True, null=True, verbose_name = "Google Street View")
    
    country = models.CharField(max_length=1, choices=TYPE_COUNTRY, verbose_name = "País")
    state = models.CharField(max_length=150, verbose_name = "Estado")
    city = models.CharField(max_length=150, verbose_name = "Ciudad")
    street = models.CharField(max_length=200, verbose_name = "Calle")
    zipcode = models.IntegerField(verbose_name = "Código Postal")
    Numint = models.IntegerField(blank=True, null=True, verbose_name = "Número Interno")
    Numext = models.IntegerField(blank=True, null=True, verbose_name = "Número Externo")
    phone = models.IntegerField(verbose_name = "Número De Contacto")
    email = models.EmailField(verbose_name = "Email")

    titleHero = models.CharField(max_length=150, verbose_name = "Título | Hero Imagen", default='Título | Hero Imagen')
    imgHero = models.ImageField(verbose_name = "Imagen Principal", upload_to="catalogo")
    descriptionimgHero = RichTextField(verbose_name = "Descripción | Hero Imagen", default='Descripción | Hero Imagen')
    
    titleimgA = models.CharField(max_length=150, verbose_name = "Título | Primera Imagen", default='Título | Primera Imagen')
    imgA = models.ImageField(blank=True, null=True, verbose_name = "Carrusel Imagen I", upload_to="catalogo")
    descriptionimgA = RichTextField(verbose_name = "Descripción | Primera Imagen", default='Descripción | Primera Imagen')

    titleimgB = models.CharField(max_length=150, verbose_name = "Título | Segunda Imagen", default='Título | Segunda Imagen')
    imgB = models.ImageField(blank=True, null=True, verbose_name = "Carrusel Imagen II", upload_to="catalogo")
    descriptionimgB = RichTextField(verbose_name = "Descripción | Segunda Imagen", default='Descripción | Segunda Imagen')

    titleimgC = models.CharField(max_length=150, verbose_name = "Título | Tercera Imagen", default='Tercera | Hero Imagen')
    imgC = models.ImageField(blank=True, null=True, verbose_name = "Carrusel Imagen III", upload_to="catalogo")
    descriptionimgC = RichTextField(verbose_name = "Descripción | Tercera Imagen", default='Descripción | Tercera Imagen')

    titleimgD = models.CharField(max_length=150, verbose_name = "Título | Cuarta Imagen", default='Título | Cuarta Imagen')
    imgD = models.ImageField(blank=True, null=True, verbose_name = "Carrusel Imagen IV", upload_to="catalogo")
    descriptionimgD = RichTextField(verbose_name = "Descripción | Cuarta Imagen",  default='Descripción | Cuarta Imagen')
   
    prototype = models.ImageField(blank=True, null=True, verbose_name = "Render")
    created = models.DateTimeField(auto_now_add=True, verbose_name = "Fecha de Creación")
    updated = models.DateTimeField(auto_now=True, verbose_name = "Fecha de Edición")
  
    # """ Traductor """ 
    class Meta: 
        verbose_name = "Inmueble"
        verbose_name_plural = "Inmuebles"
        ordering = ["-created"]
    
    def __str__(self):
        return self.title