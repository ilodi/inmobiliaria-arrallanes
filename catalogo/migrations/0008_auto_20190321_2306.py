# Generated by Django 2.1.7 on 2019-03-22 05:06

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogo', '0007_auto_20190320_2153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='catalogo',
            name='author',
            field=models.ForeignKey(blank=True, default=django.contrib.auth.models.User, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Vendedor'),
        ),
        migrations.AlterField(
            model_name='catalogo',
            name='typeC',
            field=models.ManyToManyField(related_name='get_posts', to='catalogo.Tipos_de_inmuebles', verbose_name='Tipo de Inmueble'),
        ),
    ]
