from .models import Catalogo, Tipos_de_inmuebles

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

class CatalogoListView(ListView):
    model = Catalogo
   
class CatalogoDetailView(DetailView):
    model = Catalogo
    

