from django.urls import path
from .views import CatalogoListView, CatalogoDetailView

urlpatterns = [
     path('', CatalogoListView.as_view(), name="catalogos"),
     path('<int:pk>/<slug:slug>', CatalogoDetailView.as_view(), name="catalogo"),
     # path('tipo-de-Inmueble/<int:tipo_id>/', tipo.as_view(), name="tipo"),
]

