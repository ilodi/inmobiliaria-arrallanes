from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.mail import EmailMessage 
from .forms import ContactForm


# Create your views here.

def contact(request):
  #Se instanciara antes de envialo por un diccionario de contexto
  contact_form = ContactForm()
  if request.method == "POST":
    contact_form = ContactForm(data=request.POST)
  #es valido  
    if contact_form.is_valid():
      name = request.POST.get('name', '')
      email = request.POST.get('email', '')
      whats = request.POST.get('whats', '')
      content = request.POST.get('content', '')
      #Enviamos el correo y redireccionamos
      email = EmailMessage(
        #Asunto mail
        "INMOBILIARIA ARRALLANES: Nuevo Mensaje de contacto",
        #Cuerpo del mail
        "De {} <{}>\n\nWhatsApp:{}\n\nEscribió:{}\n\n".format(name, email, whats, content),
        #Email Origen
        "no-contestar@inbox.mailtrap.io",
        #Correos a los que les llegara
        ["i92bits@gmail.com"],
        reply_to=[email]
      )

    try:
      email.send()
      #todo va muy bien salio
      return redirect(reverse('contact')+"?ok")
    except: 
       #Algo no ha ido bien
      return redirect(reverse('contact')+"?fail")

  return render(request, "contact/contacto.html", {'form':contact_form})
