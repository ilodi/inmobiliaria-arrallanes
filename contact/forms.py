from django import forms

class ContactForm(forms.Form):
  name = forms.CharField(label="Nombre", required=True, widget=forms.TextInput(
  attrs={'class':'form-control', 'id':'Nombre', 'placeholder':"Escribe tu nombre completo"}
  ),min_length=3, max_length=100)

  email = forms.EmailField(label="Email", required=True, widget=forms.EmailInput(
  attrs={'class':'form-control', 'id':'emaiL', 'placeholder':"Escribe tu email"}
  ), min_length=3, max_length=100 )

  whats = forms.DecimalField(label="Whatsapp", required=True, widget=forms.NumberInput(
  attrs={'class':'form-control', 'id':'whats', 'placeholder':"Escribe tu Whatsapp"}))

  content = forms.CharField(label="Contenido", required=True, widget=forms.Textarea(
  attrs={'class':'form-control', 'id':'mensaJ', 'rows':'3', 'placeholder':"Escribe tu mensaje"}
  ), min_length=23, max_length=1000 )