from django.contrib import admin
from django.urls import path, include
from catalogo import views as catalogo_views

#CUSTOM ADMIN
from django.conf import settings
settings

urlpatterns = [
    path('', include('core.urls')),
    path('catalogos/', include('catalogo.urls')), 
    path('page/', include('pages.urls')),
    path('contacto/', include('contact.urls')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


#Custom Title for admin
admin.site.site_header = "INMOBILIARIA ARRALLANES"
admin.site.index_title = "Panel de administrador"
admin.site.site_title = "INMOBILIARIA ARRALLANES"