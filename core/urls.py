from django.urls import path
from . import views
from .views import home, about, services
urlpatterns = [
     path('', home.as_view(), name="home"),
     path('historia/', about.as_view(), name="historia"),
     path('servicios/', services.as_view(), name="servicios"),
]

