from django.shortcuts import render
from django.views.generic.base import TemplateView

class home(TemplateView):
    template_name = "core/home.html"
    def get(self, request, *args, **kwargs):
      return render(request, self.template_name, {'title':"Inmo"})
# Create your views here.

class about(TemplateView):
    template_name = "core/historia.html"
    def get(self, request, *args, **kwargs):
      return render(request, self.template_name, {'title':"historia"})

class services(TemplateView):
    template_name = "core/servicios.html"
    def get(self, request, *args, **kwargs):
      return render(request, self.template_name, {'title':"servicios"})


